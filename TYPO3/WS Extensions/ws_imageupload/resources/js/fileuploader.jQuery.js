<!-- ###MAIN### begin -->
###JQUERY###(document).ready(function(){
    var uploader = new qq.FileUploader({
      element: document.getElementById('###ELEMENT###'),
      multiple: ###MULTIPLE###,
      allowedExtensions: [###ALLOWEDEXTENSIONS###],
      sizeLimit: ###SIZELIMIT###,
      action: '###ACTION###',
      messages: ###MESSAGES###,
      onComplete: function(id, fileName, responseJSON){
    	###ONCOMPLETE###
      }
   });
});
<!-- ###MAIN### end -->