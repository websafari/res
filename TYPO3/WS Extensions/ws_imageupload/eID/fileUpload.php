<?php
if (!defined ('TYPO3_MODE')) {
 	die ('Access denied.');
}

//Init TSFE
$TSFE = t3lib_div::makeInstance('tslib_fe',  $TYPO3_CONF_VARS, 80);
$TSFE->connectToDB();
$TSFE->initFEuser();
$TSFE->checkAlternativeIdMethods();
$TSFE->clear_preview();
$TSFE->determineId();
$TSFE->initTemplate();
$TSFE->getConfigArray();

//Write Configuration, Get, Post
$conf = $TSFE->tmpl->setup['plugin.']['tx_wsimageupload_pi1.'];
$get = t3lib_div::_GET();
$post = t3lib_div::_POST();

// list of valid extensions, ex. array("jpeg", "xml", "bmp")
$allowedExtensions = array();
// max file size in bytes
$sizeLimit = $conf['fileuploader.']['sizeLimit'];

//Make instance of uploader and handle upload
$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
$result = $uploader->handleUpload('uploads/tx_wsimageupload/');

// Return json data
echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);

/*
* Class: SimpleImage
* Author: Simon Jarvis
* Copyright: 2006 Simon Jarvis
* Date: 08/11/06
* Link: http://www.white-hat-web-design.co.uk/articles/php-image-resizing.php
*
*/ 
class SimpleImage {
 
   var $image;
   var $image_type;
 
   function load($filename) {
 
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
      if( $this->image_type == IMAGETYPE_JPEG ) {
 
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
 
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
 
         $this->image = imagecreatefrompng($filename);
      }
   }
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image,$filename);
      } elseif( $image_type == IMAGETYPE_PNG ) {
 
         imagepng($this->image,$filename);
      }
      if( $permissions != null) {
 
         chmod($filename,$permissions);
      }
   }
   function output($image_type=IMAGETYPE_JPEG) {
 
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
 
         imagegif($this->image);
      } elseif( $image_type == IMAGETYPE_PNG ) {
 
         imagepng($this->image);
      }
   }
   function getWidth() {
 
      return imagesx($this->image);
   }
   function getHeight() {
 
      return imagesy($this->image);
   }
   function resizeToHeight($height) {
 
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
 
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
 
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100;
      $this->resize($width,$height);
   }
 
   function resize($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image;
   }      
 
}

/*
* Class: Ajax Upload
* Author: Andrew Valums
* Copyright: 2010 Andrew Valums
* Link: http://github.com/valums/file-uploader
*
*/
       
/**
 * Handle file uploads via XMLHttpRequest
 */
class qqUploadedFileXhr {
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {    
        $input = fopen("php://input", "r");
        $temp = tmpfile();
        $realSize = stream_copy_to_stream($input, $temp);
        fclose($input);
        
        if ($realSize != $this->getSize()){            
            return false;
        }
        
        $target = fopen($path, "w");        
        fseek($temp, 0, SEEK_SET);
        stream_copy_to_stream($temp, $target);
        fclose($target);
        
        return true;
    }
    function getName() {
        return $_GET['qqfile'];
    }
    function getSize() {
        if (isset($_SERVER["CONTENT_LENGTH"])){
            return (int)$_SERVER["CONTENT_LENGTH"];            
        } else {
            throw new Exception('Getting content length is not supported.');
        }      
    }   
}

/**
 * Handle file uploads via regular form post (uses the $_FILES array)
 */
class qqUploadedFileForm {  
    /**
     * Save the file to the specified path
     * @return boolean TRUE on success
     */
    function save($path) {
        if(!move_uploaded_file($_FILES['qqfile']['tmp_name'], $path)){
            return false;
        }
        return true;
    }
    function getName() {
        return $_FILES['qqfile']['name'];
    }
    function getSize() {
        return $_FILES['qqfile']['size'];
    }
}

class qqFileUploader {
    private $allowedExtensions = array();
    private $sizeLimit = 10485760;
    private $file;

    function __construct(array $allowedExtensions = array(), $sizeLimit = 10485760){        
        $allowedExtensions = array_map("strtolower", $allowedExtensions);
            
        $this->allowedExtensions = $allowedExtensions;        
        $this->sizeLimit = $sizeLimit;
        
        $this->checkServerSettings();       

        if (isset($_GET['qqfile'])) {
            $this->file = new qqUploadedFileXhr();
        } elseif (isset($_FILES['qqfile'])) {
            $this->file = new qqUploadedFileForm();
        } else {
            $this->file = false; 
        }
    }
    
    private function checkServerSettings(){        
        $postSize = $this->toBytes(ini_get('post_max_size'));
        $uploadSize = $this->toBytes(ini_get('upload_max_filesize'));        
        
        if ($postSize < $this->sizeLimit || $uploadSize < $this->sizeLimit){
            $size = max(1, $this->sizeLimit / 1024 / 1024) . 'M';             
            die("{'error':'increase post_max_size and upload_max_filesize to $size'}");    
        }        
    }
    
    private function toBytes($str){
        $val = trim($str);
        $last = strtolower($str[strlen($str)-1]);
        switch($last) {
            case 'g': $val *= 1024;
            case 'm': $val *= 1024;
            case 'k': $val *= 1024;        
        }
        return $val;
    }
    
    /**
	* Clear Filenames
	*/

	function TidyFilename($val) {
    	// replace whitespace with underscore
        $new = preg_replace('=(\s+)=', '_', $val);

    	// list all umlauts 
   		$map = array(     
            '�' => 'ae',
            '�' => 'ae',
            '�'=>'ss',
            '�'=>'oe',
            '�' => 'oe',
            '�'=>'ue',
            '�'=>'ue',

            //add additional characters

            '�' => 'a',
            '�' => 'e',
            '�' => 'e',
        );

    	// convert umlauts
    	$new = str_replace(array_keys($map), array_values($map), $new);

    	// convert everything to lower
   		$new = strtolower($new);

    	// throw out all other characters
    	$new = preg_replace('#[^a-z0-9_.-]#', '', $new);

    	return $new;
	}
   
    /**
     * Returns array('success'=>true) or array('error'=>'error message')
     */
    function handleUpload($uploadDirectory, $replaceOldFile = FALSE){
        if (!is_writable($uploadDirectory)){
            return array('error' => "Server error. Upload directory isn't writable.");
        }
        
        if (!$this->file){
            return array('error' => 'No files were uploaded.');
        }
        
        $size = $this->file->getSize();
        
        if ($size == 0) {
            return array('error' => 'File is empty');
        }
        
        if ($size > $this->sizeLimit) {
            return array('error' => 'File is too large');
        }
        
        $pathinfo = pathinfo($this->file->getName());
        
        $filename = $pathinfo['filename'];
        
        $filename = $this->TidyFilename($filename);
        
        $ext = $pathinfo['extension'];

        if($this->allowedExtensions && !in_array(strtolower($ext), $this->allowedExtensions)){
            $these = implode(', ', $this->allowedExtensions);
            return array('error' => 'File has an invalid extension, it should be one of '. $these . '.');
        }
        
        if(!$replaceOldFile){
            /// don't overwrite previous files that were uploaded
            while (file_exists($uploadDirectory . $filename . '.' . $ext)) {
                $filename .= rand(10, 9999);
            }
        }
        
        if ($this->file->save($uploadDirectory . $filename . '.' . $ext)){
        	if($conf['imageHandling'] == 1) {	
	        	$image = new SimpleImage();
	        	$image->load($uploadDirectory . $filename . '.' . $ext);
	        	$image->resizeToWidth(900);
	   			/*
	        	if(isset($conf['imageHandling.']['resizeToHeight'])) $image->resizeToHeight($conf['imageHandling.']['resizeToHeight']);
	        	if(isset($conf['imageHandling.']['resizeToWidth'])) $image->resizeToWidth($conf['imageHandling.']['resizeToWidth']);
	        	if(isset($conf['imageHandling.']['scale'])) $image->scale($conf['imageHandling.']['scale']);
				if(isset($conf['imageHandling.']['resize.']['width']) && isset($conf['imageHandling.']['resize.']['height'])) $image->resize($conf['imageHandling.']['resize.']['width'], $conf['imageHandling.']['resize.']['height']);
	        	*/
	        	$image->save($uploadDirectory . $filename . '.' . $ext);
        	}
        	
            return array('success'=>true, 'filename'=>$filename . '.' . $ext);
        } else {
            return array('error'=> 'Could not save uploaded file.' .
                'The upload was cancelled, or server error encountered');
        }
        
    }    
}
?>