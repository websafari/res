<?php

########################################################################
# Extension Manager/Repository config file for ext "ws_imageupload".
#
# Auto generated 01-12-2011 14:01
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Ajax Image Upload',
	'description' => 'An ajax image uploader with image postProcessing.',
	'category' => 'fe',
	'author' => 'Florian Rachor',
	'author_email' => 'f.rachor@websafari.eu',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => 'uploads/tx_wsimageupload/',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => '',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:12:{s:9:"ChangeLog";s:4:"fbda";s:10:"README.txt";s:4:"ee2d";s:12:"ext_icon.gif";s:4:"1bdc";s:17:"ext_localconf.php";s:4:"7be2";s:14:"ext_tables.php";s:4:"e03e";s:16:"locallang_db.xml";s:4:"3eed";s:19:"doc/wizard_form.dat";s:4:"0461";s:20:"doc/wizard_form.html";s:4:"0133";s:34:"pi1/class.tx_wsimageupload_pi1.php";s:4:"469c";s:17:"pi1/locallang.xml";s:4:"578a";s:37:"static/ajax_imageupload/constants.txt";s:4:"2f43";s:33:"static/ajax_imageupload/setup.txt";s:4:"2f43";}',
);

?>