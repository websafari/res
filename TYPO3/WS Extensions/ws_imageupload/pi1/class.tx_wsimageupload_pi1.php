<?php
/***************************************************************
*  Copyright notice
*
*  (c) 2011 Florian Rachor <f.rachor@websafari.eu>
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
*
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
*
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/
/**
 * [CLASS/FUNCTION INDEX of SCRIPT]
 *
 * Hint: use extdeveval to insert/update function index above.
 */

require_once(PATH_tslib.'class.tslib_pibase.php');


/**
 * Plugin 'Ajax Imageupload' for the 'ws_imageupload' extension.
 *
 * @author	Florian Rachor <f.rachor@websafari.eu>
 * @package	TYPO3
 * @subpackage	tx_wsimageupload
 */
class tx_wsimageupload_pi1 extends tslib_pibase {
	var $prefixId      = 'tx_wsimageupload_pi1';		// Same as class name
	var $scriptRelPath = 'pi1/class.tx_wsimageupload_pi1.php';	// Path to this script relative to the extension dir.
	var $extKey        = 'ws_imageupload';	// The extension key.
	var $pi_checkCHash = true;
	
	/**
	 * The main method of the PlugIn
	 *
	 * @param	string		$content: The PlugIn content
	 * @param	array		$conf: The PlugIn configuration
	 * @return	The content that is displayed on the website
	 */
	function main($content, $conf) {
		$this->conf = $conf;
		$this->pi_setPiVarDefaults();
		$this->pi_loadLL();
		
		$GLOBALS['TSFE']->additionalHeaderData[$this->extKey] = '<script src="typo3conf/ext/ws_imageupload/resources/js/fileuploader.lib.js" type="text/javascript"></script>';
		$GLOBALS['TSFE']->setJS($this->extKey, $this->buildJS());
		
		
		$cssPath = false;
		if(file_exists($this->conf['cssFile'])) {
			$cssPath = $this->conf['cssFile'];
		}
		
		if($cssPath) {
			$GLOBALS['TSFE']->additionalHeaderData[$this->extKey] .=  '<link rel="stylesheet" type="text/css" media="all" href="'.$cssPath.'">';
		}

		$content= $this->replaceMarkers();
	
		return $this->pi_wrapInBaseClass($content);
	}
	
	function replaceMarkers(){
		$this->templateHtml = $this->cObj->fileResource($this->conf['templateFile']);
		$main = $this->cObj->getSubpart($this->templateHtml, '###MAIN###');
		
		return($main);
	}
	
	function buildJS(){
		$this->templateHtml = $this->cObj->fileResource($this->conf['templateJS']);
		$main = $this->cObj->getSubpart($this->templateHtml, '###MAIN###');
		
		$markerArray['###JQUERY###'] = 'jQuery';
		
		$keys = array_keys($this->conf['fileuploader.']);
		$i = 0;
		foreach($this->conf['fileuploader.'] as $config) {
			if(is_array($config)) {
				$subKeys = array_keys($config);
				$j = 0;
				foreach($config as $element) {
					$content .= $subKeys[$j].': "'.$element.'",'; 
					$j++;
				}
				$markerArray['###'.strtoupper(substr($keys[$i], 0, -1)).'###'] = '{'.substr($content, 0, -1).'}';
			} else $markerArray['###'.strtoupper($keys[$i]).'###'] = $config;
			$i++;
		}
		
		return $this->cObj->substituteMarkerArray($main, $markerArray);
	}
}



if (defined('TYPO3_MODE') && $TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/ws_imageupload/pi1/class.tx_wsimageupload_pi1.php'])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]['XCLASS']['ext/ws_imageupload/pi1/class.tx_wsimageupload_pi1.php']);
}

?>