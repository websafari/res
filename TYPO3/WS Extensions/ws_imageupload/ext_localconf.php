<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$TYPO3_CONF_VARS['FE']['eID_include']['wsimageuploadUpload'] = 'EXT:ws_imageupload/eID/fileUpload.php';
$TYPO3_CONF_VARS['FE']['eID_include']['wsimageuploadDelete'] = 'EXT:ws_imageupload/eID/deleteFile.php';
t3lib_extMgm::addPItoST43($_EXTKEY, 'pi1/class.tx_wsimageupload_pi1.php', '_pi1', 'list_type', 1);
?>